package com.frwk.geocode.service;

import com.frwk.geocode.model.mongo.Localizacao;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GoogleGeocodeService {


   // @Value("${google.api.key}")
    private String API_KEY;


    public Localizacao findLocalizacaoGeocode(Double latitude, Double longitude) {

        boolean addressFound = false;

        Localizacao local = new Localizacao();

        try {
            GeoApiContext context = new GeoApiContext.Builder().apiKey(API_KEY).build();

            GeocodingResult[] results = GeocodingApi.reverseGeocode(context, new LatLng(latitude.doubleValue(), longitude.doubleValue())).await();

            local.setLatitude(latitude);
            local.setLongitude(longitude);

            verifiAddress(addressFound, local, results, AddressType.STREET_ADDRESS);

            if (local.getEstado() == null) {
                verifiAddress(addressFound, local, results, AddressType.ROUTE);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return local;
    }

    private void verifiAddress(boolean addressFound, Localizacao local, GeocodingResult[] results, AddressType streetAddress) {
        for (int i = 0; i < results.length; i++) {

            AddressType[] types = results[i].types;

            for (int j = 0; j < types.length && !addressFound; j++) {

                if (types[j] == streetAddress) {

                    addressFound = true;
                    AddressComponent[] addressComponents = results[i].addressComponents;

                    for (int k = 0; k < addressComponents.length; k++) {

                        AddressComponent addressComponent = addressComponents[k];
                        AddressType addressType = getAddressType(addressComponent);

                        if (addressType != null) {

                            updateLocal(local, addressComponent, addressType);

                        }
                    }
                }

            }

        }
    }

    private void updateLocal(Localizacao local, AddressComponent addressComponent, AddressType addressType) {

        switch (addressType) {

            case ROUTE:
                local.setLogradouro(addressComponent.shortName);
                break;
            case SUBLOCALITY:
                local.setBairro(addressComponent.longName);
                break;
            case ADMINISTRATIVE_AREA_LEVEL_2:
                local.setCidade(addressComponent.longName);
                break;
            case ADMINISTRATIVE_AREA_LEVEL_1:
                local.setEstado(addressComponent.shortName);
                break;
            case STREET_NUMBER:
                local.setNumeroLogradouro(addressComponent.longName);
                break;
            case POSTAL_CODE:
                local.setCep(addressComponent.longName);
                break;

        }

    }

    private AddressType getAddressType(AddressComponent addressComponent) {

        AddressType addressType = null;
        AddressComponentType[] types = addressComponent.types;
        StringBuilder tipos = new StringBuilder();

        for (int j = 0; j < types.length; j++) {
            tipos.append(types[j]).append(" ");
        }

        if (tipos.indexOf(AddressType.ROUTE.toString()) >= 0) {

            addressType = AddressType.ROUTE;

        } else if (tipos.indexOf(AddressType.SUBLOCALITY.toString()) >= 0) {

            addressType = AddressType.SUBLOCALITY;

        } else if (tipos.indexOf(AddressType.ADMINISTRATIVE_AREA_LEVEL_2.toString()) >= 0) {

            addressType = AddressType.ADMINISTRATIVE_AREA_LEVEL_2;

        } else if (tipos.indexOf(AddressType.ADMINISTRATIVE_AREA_LEVEL_1.toString()) >= 0) {

            addressType = AddressType.ADMINISTRATIVE_AREA_LEVEL_1;

        } else if (tipos.indexOf(AddressType.STREET_NUMBER.toString()) >= 0) {

            addressType = AddressType.STREET_NUMBER;

        } else if (tipos.indexOf(AddressType.POSTAL_CODE.toString()) >= 0) {

            addressType = AddressType.POSTAL_CODE;

        }

        return addressType;

    }
}
