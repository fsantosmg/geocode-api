package com.frwk.geocode.service;

import com.frwk.geocode.model.mongo.Localizacao;
import com.frwk.geocode.repository.LocalizacaoRepository;
import com.frwk.geocode.service.util.FormatCoordenadas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MongoDBService {

    @Autowired
    LocalizacaoRepository repository;


    public Localizacao findFirstByLatitudeAndLongitude(Double latitude, Double longitude) {

        Localizacao localizacao = repository.findFirstByLatitudeAndLongitude(FormatCoordenadas.format(latitude), FormatCoordenadas.format(longitude));
        return localizacao;
    }

    public void save(Localizacao localizacao) {

        repository.save(localizacao);
    }
}
