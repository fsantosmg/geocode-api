package com.frwk.geocode.service.util;

public enum AddressTypeEnum {

    CITY("city"),
    TOWN("town");

    private final String addressType;

    AddressTypeEnum(String addressType) {
        this.addressType = addressType;

    }

    public String getAddressType(){
       return this.addressType;
    }
}
