package com.frwk.geocode;

import com.frwk.geocode.service.MongoConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeocodeApiApplication {


    public static void main(String[] args) {
        SpringApplication.run(GeocodeApiApplication.class, args);
       // MongoConverter converter = new MongoConverter();
       // converter.exec();
    }



}
