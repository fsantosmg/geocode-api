package com.frwk.geocode.service.util;

import java.util.Locale;

public class FormatCoordenadas {

    public static Double format(Double value) {

        return Double.valueOf(String.format(Locale.ROOT, "%.4f", value));
        // return Math.floor(-123.1230 * 1e4) / 1e4;
        // return Math.floor(value * Math.pow(10, 4)) / Math.pow(10, 4);


    }
}
