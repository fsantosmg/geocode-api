package com.frwk.geocode.model.mongo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("utilizacao")
@Getter@Setter
public class UtilizacaoGeocode {

    public UtilizacaoGeocode(String mensagem, String timestamp, String tempoResposta) {
        this.mensagem = mensagem;
        this.timestamp = timestamp;
        this.tempoResposta = tempoResposta;
    }

    @Id
    private String id;
    private String mensagem;
    private String timestamp;
    private String tempoResposta;
}
