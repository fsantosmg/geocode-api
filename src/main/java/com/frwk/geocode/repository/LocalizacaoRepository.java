package com.frwk.geocode.repository;

import com.frwk.geocode.model.mongo.Localizacao;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LocalizacaoRepository extends MongoRepository<Localizacao, String> {
    Localizacao findFirstByLatitudeAndLongitude(Double latitude, Double longitude);
}
