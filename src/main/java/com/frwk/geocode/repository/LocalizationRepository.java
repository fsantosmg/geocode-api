package com.frwk.geocode.repository;

import com.frwk.geocode.model.mongo.Localization;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalizationRepository extends MongoRepository<Localization, String> {
}
