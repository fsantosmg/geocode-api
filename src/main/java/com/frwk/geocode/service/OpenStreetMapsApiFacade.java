package com.frwk.geocode.service;

import com.frwk.geocode.model.mongo.Localizacao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
public class OpenStreetMapsApiFacade {

    @Value("${openstreetmap.api}")
    private String openStreetMapApi;


    public Localizacao getAddress(Double latitude, Double longitude) {
        String uri = openStreetMapApi+"/localization/find?latitude={latitude}&longitude={longitude}";

        // create auth credentials
        String authStr = "admin:p5vd1x";
        String base64Creds = Base64.getEncoder().encodeToString(authStr.getBytes());

        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);


        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> params = new HashMap<>();
        params.put("latitude", latitude.toString());
        params.put("longitude", longitude.toString());
        HttpEntity<Localizacao> entity = new HttpEntity("body", headers);
        Localizacao localizacao = restTemplate.exchange(uri, HttpMethod.GET, entity, Localizacao.class, params).getBody();
        return localizacao;

    }

}
