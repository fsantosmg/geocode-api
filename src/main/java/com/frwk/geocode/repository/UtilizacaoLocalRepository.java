package com.frwk.geocode.repository;

import com.frwk.geocode.model.mongo.UtilizacaoLocal;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UtilizacaoLocalRepository extends MongoRepository<UtilizacaoLocal, String> {
}
