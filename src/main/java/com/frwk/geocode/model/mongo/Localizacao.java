package com.frwk.geocode.model.mongo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@Document("localizacao")
@ToString
public class Localizacao {

    @Id
    private String id;
    private Double latitude;
    private Double longitude;
    private String logradouro;
    private String bairro;
    @Field("numero_logradouro")
    private String numeroLogradouro;
    private String complemento;
    private String cep;
    private String cidade;
    private String estado;


}
