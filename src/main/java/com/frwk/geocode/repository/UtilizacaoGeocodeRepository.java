package com.frwk.geocode.repository;

import com.frwk.geocode.model.mongo.UtilizacaoGeocode;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilizacaoGeocodeRepository extends MongoRepository<UtilizacaoGeocode, String> {
}
