package com.frwk.geocode.model.mongo;


import com.mongodb.client.model.geojson.Point;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;


import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@Document("localization")
public class Localization {

    @Id
    private String id;
    private Point coordenadas;
    private String logradouro;
    private String bairro;
    @Field("numero_logradouro")
    private String numeroLogradouro;
    private String complemento;
    private String cep;
    private String cidade;
    private String estado;

}
