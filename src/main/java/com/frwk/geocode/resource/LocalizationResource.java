package com.frwk.geocode.resource;

import com.frwk.geocode.model.mongo.Localizacao;
import com.frwk.geocode.service.FindAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "localization")
public class LocalizationResource {

    @Autowired
    FindAddressService findAddressService;

    @GetMapping("find")
    public ResponseEntity<Localizacao> find(@RequestParam Double latitude, @RequestParam Double longitude){

        Localizacao localizacao = findAddressService.findLocalizacao(latitude, longitude);

        return ResponseEntity.ok(localizacao);

    }

    @GetMapping("db/find")
    public ResponseEntity<Localizacao> findDb(@RequestParam Double latitude, @RequestParam Double longitude){

        Localizacao localizacao = findAddressService.findInDB(latitude, longitude);

        return ResponseEntity.ok(localizacao);

    }
}
