package com.frwk.geocode.service;

import com.frwk.geocode.model.mongo.Localizacao;
import com.frwk.geocode.model.mongo.UtilizacaoGeocode;
import com.frwk.geocode.model.mongo.UtilizacaoLocal;
import com.frwk.geocode.repository.UtilizacaoGeocodeRepository;
import com.frwk.geocode.repository.UtilizacaoLocalRepository;
import com.frwk.geocode.service.util.FormatCoordenadas;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class FindAddressService {

    @Autowired
    MongoDBService mongoDBService;

    @Autowired
    UtilizacaoGeocodeRepository utilizacaoGeocodeRepository;

    @Autowired
    UtilizacaoLocalRepository utilizacaoLocalRepository;

    @Autowired
    OpenStreetMapService openStreetMapService;

    @Autowired
    OpenStreetMapsApiFacade apiFacade;

    @Value("${api.openmaps}")
    private Boolean OPENMAPS;

    private static final Logger logger = LogManager.getLogger(FindAddressService.class);


    public Localizacao findLocalizacao(Double latitude, Double longitude) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String formatDateTime = now.format(formatter);

        Long inicio = System.currentTimeMillis();

        Localizacao localizacao = mongoDBService.findFirstByLatitudeAndLongitude(latitude, longitude);

        Long fim = System.currentTimeMillis();


        if (!OPENMAPS && localizacao == null) {
            localizacao = new Localizacao();
            localizacao.setLatitude(latitude);
            localizacao.setLongitude(longitude);
            return localizacao;
        }


        if (localizacao == null) {

            Long inicioGeo = System.currentTimeMillis();

            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // localizacao = openStreetMapService.find(latitude, longitude);
            localizacao = apiFacade.getAddress(latitude, longitude);
            Long fimGeo = System.currentTimeMillis();
            localizacao.setLatitude(FormatCoordenadas.format(latitude));
            localizacao.setLongitude(FormatCoordenadas.format(longitude));
            if (localizacao.getEstado() != null) {
                mongoDBService.save(localizacao);
                String tempoResposta = (fimGeo - inicioGeo) + " ms";
                logger.info("*********  Atualizado via Geocode em  ----" + tempoResposta);

                utilizacaoGeocodeRepository.save(new UtilizacaoGeocode("Atualizado via Geocode", formatDateTime, tempoResposta));
            }

        } else {
            String tempoResposta = Long.toString((fim - inicio)) + " ms";
            utilizacaoLocalRepository.save(new UtilizacaoLocal("Atualizado via DB local", formatDateTime, tempoResposta));
            logger.info("### Atualizado via base local em ----" + tempoResposta);
        }


        return localizacao;
    }

    public Localizacao findInDB(Double latitude, Double longitude) {
        Localizacao localizacao = mongoDBService.findFirstByLatitudeAndLongitude(latitude, longitude);

        if (localizacao == null) {
            localizacao = new Localizacao();
            localizacao.setLatitude(latitude);
            localizacao.setLongitude(longitude);
        }

        return localizacao;
    }


}
